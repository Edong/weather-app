import {ApiProperty} from '@nestjs/swagger';

export class WeatherDto {
  @ApiProperty()
  cloudPercentage: number;

  @ApiProperty()
  rainAmount: number;

  @ApiProperty()
  temperature: number;
}
