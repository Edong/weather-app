import {ApiProperty} from '@nestjs/swagger';

export class SubscribeWeatherDto {
  @ApiProperty()
  cityId: string;
}
