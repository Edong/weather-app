import {Injectable, Logger} from '@nestjs/common';
import {SocketService, WeatherApiService} from '../../shared/services';
import {WeatherInterface} from '../interfaces';
import {WeatherDto} from '../dto';

@Injectable()
export class WeatherService {
  private readonly logger = new Logger(WeatherService.name);

  constructor(private readonly weatherApiService: WeatherApiService, private readonly socketService: SocketService) {}

  public async findOne(cityId: string | number): Promise<WeatherInterface | void> {
    this.logger.debug(`[${this.findOne.name}] fetching weather for city with id ${cityId}`);
    return this.weatherApiService.findOneWeather(cityId);
  }

  public async subscribe(cityId: string | number): Promise<void> {
    this.logger.log(`[${this.subscribe.name}] subscribing to city ${cityId} weather`);
    return this.weatherApiService.subscribeToCityWeather(+cityId);
  }

  public async unsubscribe(cityId: string | number): Promise<void> {
    this.logger.log(`[${this.unsubscribe.name}] unsubscribing from city ${cityId} weather`);
    return this.weatherApiService.unsubscribeFromCityWeather(+cityId);
  }

  public async weatherChange(args: WeatherDto, cityId: string): Promise<void> {
    this.logger.log(`[${this.weatherChange.name}] weather in city ${cityId} has changed`);
    this.logger.debug(`[${this.weatherChange.name}] weather in city ${cityId} has changed: ${JSON.stringify(args)}`);
    await this.socketService.onWeatherChange(args, cityId);
  }
}
