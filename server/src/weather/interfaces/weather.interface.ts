export interface WeatherInterface {
  cloudPercentage: number;
  rainAmount: number;
  temperature: number;
}
