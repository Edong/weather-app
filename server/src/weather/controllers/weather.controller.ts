import {
  Body,
  CacheInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseInterceptors
} from '@nestjs/common';
import {WeatherService} from '../services';
import {ApiBody, ApiExcludeEndpoint, ApiOperation, ApiParam, ApiResponse, ApiTags} from '@nestjs/swagger';
import {WeatherInterface} from '../interfaces';
import {SubscribeWeatherDto, WeatherDto} from '../dto';

@Controller('weather')
@ApiTags('weather')
export class WeatherController {
  constructor(private readonly weatherService: WeatherService) {}

  @Get('/:id')
  @HttpCode(HttpStatus.OK.valueOf())
  @ApiResponse({status: 200, description: 'OK', type: [WeatherDto]})
  @ApiOperation({description: 'Get weather in given city'})
  @ApiParam({name: 'id', type: String})
  @UseInterceptors(CacheInterceptor)
  public async findOne(@Param('id') cityId: string | number): Promise<WeatherInterface | void> {
    return this.weatherService.findOne(cityId);
  }

  @Post('subscribe')
  @ApiBody({type: SubscribeWeatherDto})
  @HttpCode(HttpStatus.NO_CONTENT.valueOf())
  @ApiResponse({status: 204, description: 'NO_CONTENT'})
  @ApiOperation({description: "Subscribe to city's weather"})
  public async subscribe(@Body() {cityId}: SubscribeWeatherDto): Promise<void> {
    return this.weatherService.subscribe(cityId);
  }

  @Delete('unsubscribe')
  @ApiBody({type: SubscribeWeatherDto})
  @HttpCode(HttpStatus.NO_CONTENT.valueOf())
  @ApiResponse({status: 204, description: 'NO_CONTENT'})
  @ApiOperation({description: "Unsubscribe from city's weather"})
  public async unsubscribe(@Body() {cityId}: SubscribeWeatherDto): Promise<void> {
    return this.weatherService.unsubscribe(cityId);
  }

  @Post('/:id')
  @ApiExcludeEndpoint()
  public async create(@Param('id') cityId: string, @Body() args: WeatherDto): Promise<void> {
    this.weatherService.weatherChange(args, cityId);
  }
}
