import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {SharedModule} from './shared/shared.module';
import {CityModule} from './city/city.module';
import {WidgetModule} from './widget/widget.module';
import {WeatherModule} from './weather/weather.module';

@Module({
  imports: [SharedModule, CityModule, WidgetModule, WeatherModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
