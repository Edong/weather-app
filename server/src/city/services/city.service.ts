import {BadRequestException, Injectable, Logger} from '@nestjs/common';
import {WeatherApiService} from '../../shared/services';
import {CityInterface} from '../interfaces';
import {SearchHelper} from '../../shared/helpers';

@Injectable()
export class CityService {
  private readonly logger = new Logger(CityService.name);

  constructor(private readonly weatherApiService: WeatherApiService) {}

  //TODO: add interface
  public async findAll(skip: number, limit: number, searchTerm: string): Promise<CityInterface[] | void> {
    this.logger.debug(
      `[${this.findAll.name}] fetching cities with args skip:${skip} limit:${limit} searchTerm:${searchTerm}`
    );
    let cityList = await this.weatherApiService.findAllCities();
    if (!cityList || !cityList?.length) {
      return cityList;
    }
    if (searchTerm) {
      const cityListNames = SearchHelper.search(
        cityList.map(city => city.name),
        searchTerm
      );
      cityList = cityList.filter(({name}) => cityListNames.includes(name));
    }
    if (cityList && cityList.length) {
      return cityList.slice(skip, limit + skip);
    }
    return cityList;
  }
}
