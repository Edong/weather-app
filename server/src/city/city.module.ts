import {Module} from '@nestjs/common';
import {CityController} from './controllers';
import {CityService} from './services';

@Module({
  controllers: [CityController],
  providers: [CityService]
})
export class CityModule {}
