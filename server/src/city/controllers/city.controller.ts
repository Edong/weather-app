import {
  CacheInterceptor,
  Controller,
  DefaultValuePipe,
  Get,
  HttpCode,
  HttpStatus,
  ParseIntPipe,
  Query,
  UseInterceptors
} from '@nestjs/common';
import {CityService} from '../services';
import {ApiImplicitQuery} from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import {ApiOperation, ApiResponse, ApiTags} from '@nestjs/swagger';
import {CityDto} from '../dto';
import {CityInterface} from '../interfaces';

@Controller('city')
@ApiTags('city')
export class CityController {
  constructor(private readonly cityService: CityService) {}

  @Get()
  @HttpCode(HttpStatus.OK.valueOf())
  @ApiResponse({status: 200, description: 'OK', type: [CityDto]})
  @ApiImplicitQuery({name: 'searchTerm', required: false, type: String})
  @ApiImplicitQuery({name: 'skip', required: false, type: Number})
  @ApiImplicitQuery({name: 'limit', required: false, type: Number})
  @ApiOperation({description: 'Get available cities'})
  @UseInterceptors(CacheInterceptor)
  public async findAll(
    @Query('searchTerm', new DefaultValuePipe('')) searchTerm?: string,
    @Query('skip', new DefaultValuePipe(0), ParseIntPipe) skip?: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit?: number
  ): Promise<CityInterface[] | void> {
    // TODO: add interface
    return this.cityService.findAll(skip, limit, searchTerm);
  }
}
