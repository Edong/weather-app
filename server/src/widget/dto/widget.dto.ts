import {ApiProperty} from '@nestjs/swagger';

export class WidgetDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  cityId: string;

  @ApiProperty()
  cityName: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
