import {ApiProperty} from '@nestjs/swagger';
import {Transform} from 'class-transformer';

export class CreateWidgetDto {
  @ApiProperty()
  @Transform(({value}) => `${value}`)
  cityId: string;

  @ApiProperty()
  cityName: string;
}
