import {Module} from '@nestjs/common';
import {WidgetController} from './controllers';
import {WidgetService} from './services';
import {WeatherService} from '../weather/services';

@Module({
  controllers: [WidgetController],
  providers: [WidgetService, WeatherService]
})
export class WidgetModule {}
