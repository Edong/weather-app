import {BadRequestException, Injectable, Logger} from '@nestjs/common';
import {PrismaService} from '../../shared/services';
import {Prisma, Widget} from '../../../prisma/generated';
import WidgetCreateArgs = Prisma.WidgetCreateArgs;
import WidgetDeleteArgs = Prisma.WidgetDeleteArgs;
import {WeatherService} from '../../weather/services';

@Injectable()
export class WidgetService {
  private readonly logger = new Logger(WidgetService.name);

  constructor(private readonly prismaService: PrismaService, private readonly weatherService: WeatherService) {}

  public async findAll(): Promise<Widget[] | void> {
    this.logger.debug(`[${this.findAll.name}] fetching existing widgets.`);
    return this.prismaService.widget.findMany({}).catch(error => this.handleError(error, this.findAll.name));
  }

  public async create(args: WidgetCreateArgs): Promise<Widget | void> {
    const {
      data: {cityId}
    } = args;
    this.logger.log(`[${this.create.name}] creating new widget for city: ${cityId}`);
    this.logger.debug(`[${this.create.name}] creating new widget with args: ${JSON.stringify(args)}`);
    await this.weatherService.subscribe(cityId);
    return await this.prismaService.widget.create(args).catch(error => this.handleError(error, this.create.name));
  }

  public async delete(args: WidgetDeleteArgs): Promise<void> {
    const {
      where: {id}
    } = args;
    this.logger.log(`[${this.delete.name}] deleting widget with id: ${id}`);
    const {cityId} =
      (await this.prismaService.widget.delete(args).catch(error => this.handleError(error, this.delete.name))) || {};
    await this.weatherService.unsubscribe(cityId);
  }

  private handleError(error: Error, methodName: string): void {
    this.logger.error(`[${methodName}] widget action failed`, error.message);
    //TODO: add custome error
    throw new BadRequestException(error.message);
  }
}
