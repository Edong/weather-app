import {
  Body,
  CacheInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseInterceptors
} from '@nestjs/common';
import {WidgetService} from '../services';
import {Widget} from '../../../prisma/generated';
import {ApiBody, ApiOperation, ApiResponse, ApiTags} from '@nestjs/swagger';
import {CreateWidgetDto, WidgetDto} from '../dto';

@Controller('widget')
@ApiTags('widget')
export class WidgetController {
  constructor(private readonly widgetService: WidgetService) {}

  @Get()
  @HttpCode(HttpStatus.OK.valueOf())
  @ApiResponse({status: 200, description: 'OK'})
  @ApiOperation({description: 'Get all existing widgets'})
  @UseInterceptors(CacheInterceptor)
  public async findAll(): Promise<Widget[] | void> {
    return this.widgetService.findAll();
  }

  @Post()
  @ApiBody({type: CreateWidgetDto})
  @HttpCode(HttpStatus.CREATED.valueOf())
  @ApiResponse({status: 201, description: 'CREATED', type: [WidgetDto]})
  @ApiOperation({description: 'Create new widget'})
  public async create(@Body() args: CreateWidgetDto): Promise<Widget | void> {
    return this.widgetService.create({data: {...args}});
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT.valueOf())
  @ApiResponse({status: 204, description: 'NO_CONTENT'})
  @ApiOperation({description: 'Remove existing widget'})
  public async delete(@Param('id') id: string): Promise<void> {
    return this.widgetService.delete({where: {id}});
  }
}
