// TODO: implement tests
export class SearchHelper {
  public static search(list: string[], searchTerm: string): string[] {
    const re = RegExp(`.*${searchTerm.toLowerCase().split('').join('.*')}.*`);
    return list.filter(v => v.toLowerCase().match(re));
  }
}
