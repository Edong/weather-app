import {CacheModule, Global, HttpModule, Module} from '@nestjs/common';
import {sharedServices} from './services';
import {ConfigModule} from '@nestjs/config';
import config from './config/config';
import {WeatherGateway} from './gateway';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({load: [config], cache: true}),
    HttpModule,
    CacheModule.register({
      ttl: 15,
      max: 10
    })
  ],
  providers: [...sharedServices, WeatherGateway],
  exports: [ConfigModule, HttpModule, CacheModule, ...sharedServices]
})
export class SharedModule {}
