import * as env from 'env-var';
import {Config} from './interfaces';
import {EnvironmentEnum} from './enums';
import {createTestConfig} from './test-config';

const createConfig = (): Config => ({
  environment: env.get('ENV').required().asString(),
  weatherApiUrl: env.get('WEATHER_API_URL').required().asString(),
  weatherApiKey: env.get('WEATHER_API_KEY').required().asString(),
  subscriptionEndpointUrl: env.get('SUBSCRIPTION_ENDPOINT_URL').required().asString()
});

export default (): Config => {
  if (!process.env.ENV || process.env.ENV === EnvironmentEnum.TEST) {
    return createTestConfig();
  }
  return createConfig();
};
