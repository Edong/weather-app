import {EnvironmentEnum} from './enums';
import {Config} from './interfaces';

export const createTestConfig = (): Config => ({
  environment: EnvironmentEnum.TEST,
  weatherApiUrl: 'http://localhost:3002/api',
  weatherApiKey: 'ba721f9895d5cebe18697546d08580b3bd7faee8',
  subscriptionEndpointUrl: 'http://localhost:3001/weather'
});
