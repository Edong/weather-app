export interface Config {
  environment: string;
  weatherApiUrl: string;
  weatherApiKey: string;
  subscriptionEndpointUrl: string;
}
