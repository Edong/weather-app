export enum EnvironmentEnum {
  TEST = 'test',
  DEV = 'dev',
  STAGE = 'stage'
}
