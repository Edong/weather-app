import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer
} from '@nestjs/websockets';
import {Logger} from '@nestjs/common';
import {Socket, Server} from 'socket.io';
import {SocketService} from '../services';

@WebSocketGateway()
export class WeatherGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  private readonly logger = new Logger(WeatherGateway.name);

  constructor(private readonly socketService: SocketService) {}

  @WebSocketServer()
  server: Server;

  public afterInit(server: Server): void {
    this.logger.log(`[${this.afterInit.name}] assigning server to socket service`);
    this.socketService.socket = server;
  }

  public handleConnection(client: Socket): void {
    this.logger.log(`[${this.handleConnection.name}] client connected: ${client.id}`);
  }

  public handleDisconnect(client: Socket): void {
    this.logger.log(`[${this.handleDisconnect.name}] client disconnected: ${client.id}`);
  }
}
