import {Injectable, Logger} from '@nestjs/common';
import {Server} from 'socket.io';
import {WeatherDto} from '../../weather/dto';

@Injectable()
export class SocketService {
  private readonly logger = new Logger(SocketService.name);

  public socket: Server = null;

  public onWeatherChange(args: WeatherDto, cityId): void {
    this.logger.log(`[${this.onWeatherChange.name}] emitting weather change event for city: ${cityId}`);
    this.socket.emit('weather-change', {...args, cityId});
  }
}
