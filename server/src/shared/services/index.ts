import {WeatherApiService} from './weather-api.service';
import {PrismaService} from './prisma.service';
import {SocketService} from './socket.service';

export * from './weather-api.service';
export * from './prisma.service';
export * from './socket.service';

export const sharedServices = [WeatherApiService, PrismaService, SocketService];
