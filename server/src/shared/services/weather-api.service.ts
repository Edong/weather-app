import {BadRequestException, HttpService, Injectable, Logger} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {CityInterface} from '../../city/interfaces';
import {WeatherInterface} from '../../weather/interfaces';

enum WeatherApiEndpointsEnum {
  FIND_ALL_CITIES = '/city-list',
  FIND_ONE_WEATHER = '/weather',
  SUBSCRIBE_TO_WEATHER = '/hooks/weather/subscribe',
  UNSUBSCRIBE_FROM_WEATHER = '/hooks/weather/unsubscribe'
}

@Injectable()
export class WeatherApiService {
  private readonly logger = new Logger(WeatherApiService.name);

  constructor(private readonly httpService: HttpService, private readonly configService: ConfigService) {}

  public async findAllCities(): Promise<CityInterface[] | void> {
    this.logger.debug(`[${this.findAllCities.name}] fetching cities from weather-api`);
    return this.getRequest<CityInterface[]>(WeatherApiEndpointsEnum.FIND_ALL_CITIES);
  }

  public async findOneWeather(cityId: string | number): Promise<WeatherInterface | void> {
    this.logger.debug(`[${this.findOneWeather.name}] fetching weather for city with id ${cityId}`);
    return this.getRequest<WeatherInterface>(WeatherApiEndpointsEnum.FIND_ONE_WEATHER, cityId);
  }

  public async subscribeToCityWeather(cityId: number): Promise<void> {
    this.logger.log(`[${this.subscribeToCityWeather.name}] subscribing to city ${cityId} weather`);
    return this.postRequest<void>(WeatherApiEndpointsEnum.SUBSCRIBE_TO_WEATHER, {
      cityId,
      url: `${this.configService.get<string>('subscriptionEndpointUrl')}/${cityId}`
    });
  }

  public async unsubscribeFromCityWeather(cityId: number): Promise<void> {
    this.logger.log(`[${this.unsubscribeFromCityWeather.name}] unsubscribing from city ${cityId} weather`);
    return this.postRequest<void>(`${WeatherApiEndpointsEnum.UNSUBSCRIBE_FROM_WEATHER}/${cityId}`);
  }

  private async getRequest<T>(endpoint: WeatherApiEndpointsEnum, param?: string | number): Promise<T | void> {
    let url = `${this.configService.get<string>('weatherApiUrl')}${endpoint}`;
    if (param) url = `${url}/${param}`;
    this.logger.debug(`[${this.getRequest.name}] fetching data from weather-api endpoint: ${url}`);
    try {
      const {data} = await this.httpService.get(url, this.getHeaders()).toPromise();
      return data;
    } catch (error) {
      this.handleError(error, this.getRequest.name);
    }
  }

  private async postRequest<T>(
    endpoint: WeatherApiEndpointsEnum | string,
    payload?: Record<string, any>
  ): Promise<T | void> {
    let url = `${this.configService.get<string>('weatherApiUrl')}${endpoint}`;
    this.logger.log(`[${this.postRequest.name}] sending post request to weather-api endpoint: ${url}`);
    this.logger.debug(
      `[${this.postRequest.name}] sending post request to weather-api endpoint: ${url}, with payload: ${JSON.stringify(
        payload
      )}`
    );
    try {
      const {data} = await this.httpService.post(url, payload, this.getHeaders()).toPromise();
      return data;
    } catch (error) {
      this.handleError(error, this.postRequest.name);
    }
  }

  private handleError(error: Error, methodName: string): void {
    this.logger.error(`[${methodName}] weather-api request failed`, error.message);
    //TODO: add custome error
    throw new BadRequestException(error.message);
  }

  private getHeaders(): Record<string, any> {
    return {
      headers: {Authorization: `Bearer ${this.configService.get<string>('weatherApiKey')}`}
    };
  }
}
