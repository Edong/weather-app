/*
  Warnings:

  - Added the required column `cityId` to the `widgets` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "widgets" ADD COLUMN     "cityId" TEXT NOT NULL;
