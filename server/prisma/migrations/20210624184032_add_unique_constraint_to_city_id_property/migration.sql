/*
  Warnings:

  - A unique constraint covering the columns `[cityId]` on the table `widgets` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "City_id_unique_constraint" ON "widgets"("cityId");
