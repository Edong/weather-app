/*
  Warnings:

  - Added the required column `cityName` to the `widgets` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "widgets" ADD COLUMN     "cityName" TEXT NOT NULL;
