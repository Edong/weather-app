module.exports = {
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error'
  },
  extends: ['prettier'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  env: {
    es6: true,
    browser: true,
    es2021: true
  }
};
