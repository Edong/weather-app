import {Test, TestingModule} from '@nestjs/testing';
import {SharedModule} from '../../../src/shared/shared.module';
import {WeatherController} from '../../../src/weather/controllers';
import {WeatherService} from '../../../src/weather/services';
import {BadRequestException} from '@nestjs/common';

describe('WeatherController', () => {
  let weatherController: WeatherController;
  let weatherService: WeatherService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule],
      controllers: [WeatherController],
      providers: [WeatherService]
    }).compile();

    weatherController = app.get<WeatherController>(WeatherController);
    weatherService = app.get<WeatherService>(WeatherService);
  });

  const testWeather = {cloudPercentage: 83, rainAmount: 5.5, temperature: 22.6};

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('findOne', () => {
    it('should fetch weather for given city with given string id', async () => {
      const findWeatherSpy = jest.spyOn(weatherService, 'findOne').mockResolvedValue(testWeather);
      const result = await weatherController.findOne('123');
      expect(result).toEqual(testWeather);
      expect(findWeatherSpy).toHaveBeenCalledWith('123');
      expect(findWeatherSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch weather for given city with given number id', async () => {
      const findWeatherSpy = jest.spyOn(weatherService, 'findOne').mockResolvedValue(testWeather);
      const result = await weatherController.findOne(123);
      expect(result).toEqual(testWeather);
      expect(findWeatherSpy).toHaveBeenCalledWith(123);
      expect(findWeatherSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch weather with invalid city id and throw error', async () => {
      const findWeatherSpy = jest.spyOn(weatherService, 'findOne').mockRejectedValue(new BadRequestException());
      const result = weatherController.findOne('invalid');
      await expect(result).rejects.toHaveProperty(['status'], 400);
      await expect(result).rejects.toHaveProperty(['message'], 'Bad Request');
      expect(findWeatherSpy).toHaveBeenCalledWith('invalid');
      expect(findWeatherSpy).toHaveBeenCalledTimes(1);
    });
  });
});
