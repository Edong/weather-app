import {Test, TestingModule} from '@nestjs/testing';
import {SharedModule} from '../../../src/shared/shared.module';
import {WeatherService} from '../../../src/weather/services';
import {BadRequestException} from '@nestjs/common';
import {WeatherApiService} from '../../../src/shared/services';

describe('WeatherService', () => {
  let weatherApiService: WeatherApiService;
  let weatherService: WeatherService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule],
      providers: [WeatherService]
    }).compile();

    weatherApiService = app.get<WeatherApiService>(WeatherApiService);
    weatherService = app.get<WeatherService>(WeatherService);
  });

  const testWeather = {cloudPercentage: 83, rainAmount: 5.5, temperature: 22.6};

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('findOne', () => {
    it('should fetch weather for given city with given string id', async () => {
      const findOneWeatherSpy = jest.spyOn(weatherApiService, 'findOneWeather').mockResolvedValue(testWeather);
      const result = await weatherService.findOne('123');
      expect(result).toEqual(testWeather);
      expect(findOneWeatherSpy).toHaveBeenCalledWith('123');
      expect(findOneWeatherSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch weather for given city with given number id', async () => {
      const findOneWeatherSpy = jest.spyOn(weatherApiService, 'findOneWeather').mockResolvedValue(testWeather);
      const result = await weatherService.findOne(123);
      expect(result).toEqual(testWeather);
      expect(findOneWeatherSpy).toHaveBeenCalledWith(123);
      expect(findOneWeatherSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch weather with invalid city id and throw error', async () => {
      const findOneWeatherSpy = jest
        .spyOn(weatherApiService, 'findOneWeather')
        .mockRejectedValue(new BadRequestException());
      const result = weatherService.findOne('invalid');
      await expect(result).rejects.toHaveProperty(['status'], 400);
      await expect(result).rejects.toHaveProperty(['message'], 'Bad Request');
      expect(findOneWeatherSpy).toHaveBeenCalledWith('invalid');
      expect(findOneWeatherSpy).toHaveBeenCalledTimes(1);
    });
  });
});
