import {Test, TestingModule} from '@nestjs/testing';
import {SharedModule} from '../../../src/shared/shared.module';
import {WeatherApiService} from '../../../src/shared/services';
import {HttpService} from '@nestjs/common';
import {of} from 'rxjs';

describe('WeatherApiService', () => {
  let weatherApiService: WeatherApiService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule]
    }).compile();

    weatherApiService = app.get<WeatherApiService>(WeatherApiService);
  });

  const testWeather = {cloudPercentage: 83, rainAmount: 5.5, temperature: 22.6};
  const testCities = [
    {id: 123, name: 'TestCity1'},
    {id: 234, name: 'TestCity2'},
    {id: 345, name: 'TestCity3'},
    {id: 456, name: 'TestCity4'},
    {id: 567, name: 'TestCity5'},
    {id: 678, name: 'TestCity6'},
    {id: 789, name: 'TestCity7'},
    {id: 890, name: 'TestCity8'},
    {id: 901, name: 'TestCity9'},
    {id: 112, name: 'TestCity10'}
  ];
  const url = 'http://localhost:3002/api';
  const apiKey = 'ba721f9895d5cebe18697546d08580b3bd7faee8';

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('findAllCities', () => {
    const response = {
      data: testCities,
      headers: {},
      config: {url: 'http://localhost:3002/api/city-list'},
      status: 200,
      statusText: 'OK'
    };

    it('should fetch all cities', async () => {
      const httpServiceSpy = jest.spyOn(HttpService.prototype as any, 'get').mockImplementation(() => of(response));
      const result = await weatherApiService.findAllCities();
      expect(result).toEqual(testCities);
      expect(httpServiceSpy).toHaveBeenCalledWith(`${url}/city-list`, {headers: {Authorization: `Bearer ${apiKey}`}});
      expect(httpServiceSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('findOneWeather', () => {
    const response = {
      data: testWeather,
      headers: {},
      config: {url: 'http://localhost:3002/api/weather/123'},
      status: 200,
      statusText: 'OK'
    };

    it('should fetch weather', async () => {
      const httpServiceSpy = jest.spyOn(HttpService.prototype as any, 'get').mockImplementation(() => of(response));
      const result = await weatherApiService.findOneWeather(123);
      expect(result).toEqual(testWeather);
      expect(httpServiceSpy).toHaveBeenCalledWith(`${url}/weather/123`, {headers: {Authorization: `Bearer ${apiKey}`}});
      expect(httpServiceSpy).toHaveBeenCalledTimes(1);
    });
  });
});
