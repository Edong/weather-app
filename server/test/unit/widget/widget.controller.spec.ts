import {Test, TestingModule} from '@nestjs/testing';
import {SharedModule} from '../../../src/shared/shared.module';
import {WidgetController} from '../../../src/widget/controllers';
import {WidgetService} from '../../../src/widget/services';
import {WeatherService} from '../../../src/weather/services';

describe('WidgetController', () => {
  let widgetController: WidgetController;
  let widgetService: WidgetService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule],
      controllers: [WidgetController],
      providers: [WidgetService, WeatherService]
    }).compile();

    widgetController = app.get<WidgetController>(WidgetController);
    widgetService = app.get<WidgetService>(WidgetService);
  });

  const currentDate = new Date();
  const testWidgets = [
    {id: '123', createdAt: currentDate, updatedAt: currentDate, cityId: '321', cityName: 'cityName'},
    {id: '234', createdAt: currentDate, updatedAt: currentDate, cityId: '432', cityName: 'cityName'},
    {id: '345', createdAt: currentDate, updatedAt: currentDate, cityId: '543', cityName: 'cityName'},
    {id: '456', createdAt: currentDate, updatedAt: currentDate, cityId: '654', cityName: 'cityName'}
  ];

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('findAll', () => {
    it('should fetch existing widgets', async () => {
      const findWidgetsSpy = jest.spyOn(widgetService, 'findAll').mockResolvedValue(testWidgets);
      const result = await widgetController.findAll();
      expect(result).toEqual(testWidgets);
      expect(findWidgetsSpy).toHaveBeenCalledWith();
      expect(findWidgetsSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch existing widgets and return empty array', async () => {
      const findWidgetsSpy = jest.spyOn(widgetService, 'findAll').mockResolvedValue([]);
      const result = await widgetController.findAll();
      expect(result).toEqual([]);
      expect(findWidgetsSpy).toHaveBeenCalledWith();
      expect(findWidgetsSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('create', () => {
    it('should create widget', async () => {
      const createWidgetSpy = jest.spyOn(widgetService, 'create').mockResolvedValue(testWidgets[0]);
      const result = await widgetController.create({cityId: '999', cityName: 'cityName'});
      expect(result).toEqual(testWidgets[0]);
      expect(createWidgetSpy).toHaveBeenCalledWith({data: {cityId: '999', cityName: 'cityName'}});
      expect(createWidgetSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('delete', () => {
    it('should delete widget', async () => {
      const deleteWidgetSpy = jest.spyOn(widgetService, 'delete').mockResolvedValue(undefined);
      const result = await widgetController.delete('666');
      expect(result).toEqual(undefined);
      expect(deleteWidgetSpy).toHaveBeenCalledWith({where: {id: '666'}});
      expect(deleteWidgetSpy).toHaveBeenCalledTimes(1);
    });
  });
});
