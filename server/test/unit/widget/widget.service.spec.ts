import {Test, TestingModule} from '@nestjs/testing';
import {SharedModule} from '../../../src/shared/shared.module';
import {WidgetService} from '../../../src/widget/services';
import {PrismaService} from '../../../src/shared/services';
import {BadRequestException} from '@nestjs/common';
import {WeatherService} from '../../../src/weather/services';
import {Widget} from '../../../prisma/generated';

describe('WidgetService', () => {
  let widgetService: WidgetService;
  let weatherService: WeatherService;
  let prismaService: PrismaService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule],
      providers: [WidgetService, WeatherService]
    }).compile();

    prismaService = app.get<PrismaService>(PrismaService);
    widgetService = app.get<WidgetService>(WidgetService);
    weatherService = app.get<WeatherService>(WeatherService);
  });

  const currentDate = new Date();
  const testWidgets = [
    {id: '123', createdAt: currentDate, updatedAt: currentDate, cityId: '321', cityName: 'cityName'},
    {id: '234', createdAt: currentDate, updatedAt: currentDate, cityId: '432', cityName: 'cityName'},
    {id: '345', createdAt: currentDate, updatedAt: currentDate, cityId: '543', cityName: 'cityName'},
    {id: '456', createdAt: currentDate, updatedAt: currentDate, cityId: '654', cityName: 'cityName'}
  ];

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('findAll', () => {
    it('should fetch existing widgets', async () => {
      const findManySpy = jest.spyOn(prismaService.widget, 'findMany').mockResolvedValue(testWidgets);
      const result = await widgetService.findAll();
      expect(result).toEqual(testWidgets);
      expect(findManySpy).toHaveBeenCalledWith({});
      expect(findManySpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch existing widgets and return empty array', async () => {
      const findManySpy = jest.spyOn(prismaService.widget, 'findMany').mockResolvedValue([]);
      const result = await widgetService.findAll();
      expect(result).toEqual([]);
      expect(findManySpy).toHaveBeenCalledWith({});
      expect(findManySpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch widget and throw error', async () => {
      const findManySpy = jest.spyOn(prismaService.widget, 'findMany').mockRejectedValue(new BadRequestException());
      const result = widgetService.findAll();
      await expect(result).rejects.toHaveProperty(['status'], 400);
      await expect(result).rejects.toHaveProperty(['message'], 'Bad Request');
      expect(findManySpy).toHaveBeenCalledWith({});
      expect(findManySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('create', () => {
    it('should create widget', async () => {
      const createSpy = jest.spyOn(prismaService.widget, 'create').mockResolvedValue(testWidgets[0]);
      const subscribeSpy = jest.spyOn(weatherService, 'subscribe').mockResolvedValue(undefined);
      const result = await widgetService.create({data: {cityId: '999', cityName: 'cityName'}});
      expect(result).toEqual(testWidgets[0]);
      expect(createSpy).toHaveBeenCalledWith({data: {cityId: '999', cityName: 'cityName'}});
      expect(createSpy).toHaveBeenCalledTimes(1);
      expect(subscribeSpy).toHaveBeenCalledTimes(1);
      expect(subscribeSpy).toHaveBeenCalledWith('999');
    });
  });

  describe('delete', () => {
    it('should delete widget', async () => {
      const deleteSpy = jest.spyOn(prismaService.widget, 'delete').mockResolvedValue({cityId: 'cityId'} as Widget);
      const unsubscribeSpy = jest.spyOn(weatherService, 'unsubscribe').mockResolvedValue(undefined);
      const result = await widgetService.delete({where: {id: '666'}});
      expect(result).toEqual(undefined);
      expect(deleteSpy).toHaveBeenCalledWith({where: {id: '666'}});
      expect(deleteSpy).toHaveBeenCalledTimes(1);
      expect(unsubscribeSpy).toHaveBeenCalledTimes(1);
      expect(unsubscribeSpy).toHaveBeenCalledWith('cityId');
    });
  });
});
