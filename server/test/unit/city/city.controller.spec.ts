import {Test, TestingModule} from '@nestjs/testing';
import {CityController} from '../../../src/city/controllers';
import {CityService} from '../../../src/city/services';
import {SharedModule} from '../../../src/shared/shared.module';

describe('CityController', () => {
  let cityController: CityController;
  let cityService: CityService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule],
      controllers: [CityController],
      providers: [CityService]
    }).compile();

    cityController = app.get<CityController>(CityController);
    cityService = app.get<CityService>(CityService);
  });

  const testCities = [
    {id: 123, name: 'TestCity1'},
    {id: 234, name: 'TestCity2'},
    {id: 345, name: 'TestCity3'},
    {id: 456, name: 'TestCity4'},
    {id: 567, name: 'TestCity5'}
  ];

  afterEach(() => {
    jest.restoreAllMocks();
  });

  // TODO: add search test
  describe('findAll', () => {
    it('should fetch available cities without pagination arguments', async () => {
      const findCitiesSpy = jest.spyOn(cityService, 'findAll').mockResolvedValue(testCities);
      const result = await cityController.findAll();
      expect(result).toEqual(testCities);
      expect(findCitiesSpy).toHaveBeenCalledWith(undefined, undefined, undefined);
      expect(findCitiesSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch available cities with pagination arguments', async () => {
      const findCitiesSpy = jest.spyOn(cityService, 'findAll').mockResolvedValue(testCities);
      const result = await cityController.findAll('', 2, 10);
      expect(result).toEqual(testCities);
      expect(findCitiesSpy).toHaveBeenCalledWith(2, 10, '');
      expect(findCitiesSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch available cities and return empty array', async () => {
      const findCitiesSpy = jest.spyOn(cityService, 'findAll').mockResolvedValue([]);
      const result = await cityController.findAll('', 10, 15);
      expect(result).toEqual([]);
      expect(findCitiesSpy).toHaveBeenCalledWith(10, 15, '');
      expect(findCitiesSpy).toHaveBeenCalledTimes(1);
    });
  });
});
