import {CityService} from '../../../src/city/services';
import {Test, TestingModule} from '@nestjs/testing';
import {SharedModule} from '../../../src/shared/shared.module';
import {WeatherApiService} from '../../../src/shared/services';

describe('CityService', () => {
  let cityService: CityService;
  let weatherApiService: WeatherApiService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [SharedModule],
      providers: [CityService]
    }).compile();

    cityService = app.get<CityService>(CityService);
    weatherApiService = app.get<WeatherApiService>(WeatherApiService);
  });

  const testCities = [
    {id: 123, name: 'TestCity1'},
    {id: 234, name: 'TestCity2'},
    {id: 345, name: 'TestCity3'},
    {id: 456, name: 'TestCity4'},
    {id: 567, name: 'TestCity5'},
    {id: 678, name: 'TestCity6'},
    {id: 789, name: 'TestCity7'},
    {id: 890, name: 'TestCity8'},
    {id: 901, name: 'TestCity9'},
    {id: 112, name: 'TestCity10'}
  ];

  afterEach(() => {
    jest.restoreAllMocks();
  });

  // TODO: add search test
  describe('findAll', () => {
    it.each`
      skip   | limit | expectedResult
      ${0}   | ${10} | ${testCities}
      ${1}   | ${1}  | ${[{id: 234, name: 'TestCity2'}]}
      ${5}   | ${2}  | ${[{id: 678, name: 'TestCity6'}, {id: 789, name: 'TestCity7'}]}
      ${7}   | ${3}  | ${[{id: 890, name: 'TestCity8'}, {id: 901, name: 'TestCity9'}, {id: 112, name: 'TestCity10'}]}
      ${100} | ${1}  | ${[]}
      ${0}   | ${0}  | ${[]}
    `(
      'should fetch available cities with skip %j, limit %j and expected outcome %j',
      async ({skip, limit, expectedResult}) => {
        const findAllCitiesSpy = jest.spyOn(weatherApiService, 'findAllCities').mockResolvedValue(testCities);
        const result = await cityService.findAll(skip, limit, '');
        expect(result).toEqual(expectedResult);
        expect(findAllCitiesSpy).toHaveBeenCalledWith();
        expect(findAllCitiesSpy).toHaveBeenCalledTimes(1);
      }
    );
  });
});
