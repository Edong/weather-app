import {CACHE_MANAGER, HttpService, INestApplication} from '@nestjs/common';
import * as request from 'supertest';
import {PrismaDelegatesEnum, TestUtils} from '../helpers';
import {Widget} from '../../../prisma/generated';
import {Cache} from 'cache-manager';
import {of} from 'rxjs';

describe('WidgetController (e2e)', () => {
  let app: INestApplication;
  let cacheManager: Cache;

  beforeAll(async () => {
    app = await TestUtils.createApp();
    cacheManager = app.get<Cache>(CACHE_MANAGER);
  });

  const currentDate = new Date();
  const testWidgets = [
    {id: '123', createdAt: currentDate, updatedAt: currentDate, cityId: '321', cityName: 'cityName'},
    {id: '234', createdAt: currentDate, updatedAt: currentDate, cityId: '432', cityName: 'cityName'},
    {id: '345', createdAt: currentDate, updatedAt: currentDate, cityId: '543', cityName: 'cityName'}
  ];
  const postResponse = {
    data: null,
    headers: {},
    config: {url: 'http://localhost:3002/api/hooks/weather/subscribe'},
    status: 204,
    statusText: 'NO_CONTENT'
  };

  afterEach(async () => {
    jest.restoreAllMocks();
    await TestUtils.dropRecords();
    await cacheManager.reset();
  });

  afterAll(async () => {
    await app.close();
  });

  // TODO: add error tests
  it('/widget (POST)', async done => {
    // TODO: check given args
    jest.spyOn(HttpService.prototype as any, 'post').mockImplementation(() => of(postResponse));

    const response = await request(app.getHttpServer())
      .post(`/widget`)
      .send({cityId: '123', cityName: 'cityName'})
      .expect(201);
    const widget = await TestUtils.getRecord<Widget>(PrismaDelegatesEnum.WIDGET, {});
    expect(response.body).toEqual({
      ...widget,
      createdAt: widget.createdAt.toISOString(),
      updatedAt: widget.updatedAt.toISOString()
    });
    done();
  });

  // TODO: add error tests
  it('/widget (DELETE)', async done => {
    // TODO: check given args
    jest.spyOn(HttpService.prototype as any, 'post').mockImplementation(() => of(postResponse));

    await TestUtils.insertRecord(PrismaDelegatesEnum.WIDGET, testWidgets[0]);
    const response = await request(app.getHttpServer()).delete('/widget/123').expect(204);
    await expect(response.body).toEqual({});
    done();
  });

  // TODO: add error tests
  it('/widget (GET) single', async () => {
    await TestUtils.insertRecord(PrismaDelegatesEnum.WIDGET, testWidgets[0]);
    return request(app.getHttpServer())
      .get(`/widget`)
      .expect(200)
      .expect([
        {
          id: testWidgets[0].id,
          cityId: testWidgets[0].cityId,
          createdAt: testWidgets[0].createdAt.toISOString(),
          updatedAt: testWidgets[0].updatedAt.toISOString(),
          cityName: testWidgets[0].cityName
        }
      ]);
  });

  // TODO: add error tests
  it('/widget (GET) multiple', async () => {
    for (const widget of testWidgets) {
      await TestUtils.insertRecord(PrismaDelegatesEnum.WIDGET, widget);
    }
    return request(app.getHttpServer())
      .get(`/widget`)
      .expect(200)
      .expect([
        ...testWidgets.map(widget => {
          widget.createdAt = widget.createdAt.toISOString() as any;
          widget.updatedAt = widget.updatedAt.toISOString() as any;
          return widget;
        })
      ]);
  });
});
