import {PrismaService} from '../../../src/shared/services';
import {Prisma} from '../../../prisma/generated';
import {Test, TestingModule} from '@nestjs/testing';
import {AppModule} from '../../../src/app.module';
import {INestApplication} from '@nestjs/common';

// NOTE: Order is important because of lack cascade delete https://github.com/prisma/prisma/issues/2810
export enum PrismaDelegatesEnum {
  WIDGET = 'widget'
}

export class TestUtils {
  private static prisma = new PrismaService();

  public static async createApp(): Promise<INestApplication> {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule]
    }).compile();
    const app = moduleFixture.createNestApplication();
    return app.init();
  }

  public static async dropRecords(): Promise<void> {
    const promises = (<any>Object).values(PrismaDelegatesEnum).map(delegate => this.prisma[delegate].deleteMany({}));
    await Promise.all(promises);
  }

  public static async getRecord<T>(prismaDelegate: PrismaDelegatesEnum, where: Record<string, any>): Promise<T> {
    return this.prisma[prismaDelegate as unknown as Prisma.ModelName].findFirst({where});
  }

  public static async insertRecord<T>(prismaDelegate: PrismaDelegatesEnum, data: Record<string, any>): Promise<T> {
    return this.prisma[prismaDelegate as unknown as Prisma.ModelName].create({data});
  }
}
