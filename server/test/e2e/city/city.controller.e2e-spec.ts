import {CACHE_MANAGER, HttpService, INestApplication} from '@nestjs/common';
import * as request from 'supertest';
import {of} from 'rxjs';
import {TestUtils} from '../helpers';
import {Cache} from 'cache-manager';

describe('CityController (e2e)', () => {
  let app: INestApplication;
  let cacheManager: Cache;

  beforeAll(async () => {
    app = await TestUtils.createApp();
    cacheManager = app.get<Cache>(CACHE_MANAGER);
  });

  const testCities = [
    {id: 123, name: 'TestCity1'},
    {id: 234, name: 'TestCity2'},
    {id: 345, name: 'TestCity3'},
    {id: 456, name: 'TestCity4'},
    {id: 567, name: 'TestCity5'},
    {id: 678, name: 'TestCity6'},
    {id: 789, name: 'TestCity7'},
    {id: 890, name: 'TestCity8'},
    {id: 901, name: 'TestCity9'},
    {id: 112, name: 'TestCity10'}
  ];
  const response = {
    data: testCities,
    headers: {},
    config: {url: 'http://localhost:3002/api/city-list'},
    status: 200,
    statusText: 'OK'
  };

  afterEach(async () => {
    jest.restoreAllMocks();
    await cacheManager.reset();
  });

  afterAll(async () => {
    await app.close();
  });

  // TODO: add error tests
  // TODO: add search test
  it.each`
    skip   | limit | expectedResult
    ${0}   | ${10} | ${testCities}
    ${1}   | ${1}  | ${[{id: 234, name: 'TestCity2'}]}
    ${5}   | ${2}  | ${[{id: 678, name: 'TestCity6'}, {id: 789, name: 'TestCity7'}]}
    ${7}   | ${3}  | ${[{id: 890, name: 'TestCity8'}, {id: 901, name: 'TestCity9'}, {id: 112, name: 'TestCity10'}]}
    ${100} | ${1}  | ${[]}
    ${0}   | ${0}  | ${[]}
  `('/city (GET)', ({skip, limit, expectedResult}) => {
    jest.spyOn(HttpService.prototype as any, 'get').mockImplementation(() => of(response));

    return request(app.getHttpServer()).get(`/city?skip=${skip}&limit=${limit}`).expect(200).expect(expectedResult);
  });
});
