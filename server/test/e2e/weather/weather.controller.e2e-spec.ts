import {CACHE_MANAGER, HttpService, INestApplication} from '@nestjs/common';
import * as request from 'supertest';
import {of} from 'rxjs';
import {TestUtils} from '../helpers';
import {Cache} from 'cache-manager';

describe('WeatherController (e2e)', () => {
  let app: INestApplication;
  let cacheManager: Cache;

  beforeAll(async () => {
    app = await TestUtils.createApp();
    cacheManager = app.get<Cache>(CACHE_MANAGER);
  });

  const testWeather = {cloudPercentage: 83, rainAmount: 5.5, temperature: 22.6};
  const response = {
    data: testWeather,
    headers: {},
    config: {url: 'http://localhost:3002/api/city-list'},
    status: 200,
    statusText: 'OK'
  };
  const subscribeResponse = {
    data: {},
    headers: {},
    config: {url: 'http://localhost:3002/api/hooks/weather/subscribe'},
    status: 204,
    statusText: 'NO_CONTENT'
  };
  const unsubscribeResponse = {
    data: {},
    headers: {},
    config: {url: 'http://localhost:3002/api/hooks/weather/subscribe'},
    status: 204,
    statusText: 'NO_CONTENT'
  };

  afterEach(async () => {
    jest.restoreAllMocks();
    jest.clearAllMocks();
    await cacheManager.reset();
  });

  afterAll(async () => {
    await app.close();
  });

  // TODO: add error tests
  it('/weather/:id (GET)', () => {
    jest.spyOn(HttpService.prototype as any, 'get').mockImplementationOnce(() => of(response));

    return request(app.getHttpServer()).get(`/weather/123`).expect(200).expect(testWeather);
  });

  // TODO: add error tests
  it('/weather/subscribe (POST)', async done => {
    const postSpy = jest
      .spyOn(HttpService.prototype as any, 'post')
      .mockImplementationOnce(() => of(subscribeResponse));

    await request(app.getHttpServer()).post(`/weather/subscribe`).send({cityId: 123}).expect(204).expect({});
    expect(postSpy).toHaveBeenCalledWith(
      'http://localhost:3002/api/hooks/weather/subscribe',
      {cityId: 123, url: 'http://localhost:3001/weather/123'},
      {headers: {Authorization: 'Bearer ba721f9895d5cebe18697546d08580b3bd7faee8'}}
    );
    done();
  });

  // TODO: add error tests
  it('/weather/unsubscribe (DELETE)', async done => {
    const postSpy = jest
      .spyOn(HttpService.prototype as any, 'post')
      .mockImplementationOnce(() => of(unsubscribeResponse));

    await request(app.getHttpServer()).delete(`/weather/unsubscribe`).send({cityId: 123}).expect(204).expect({});
    expect(postSpy).toHaveBeenCalledWith('http://localhost:3002/api/hooks/weather/unsubscribe/123', undefined, {
      headers: {Authorization: 'Bearer ba721f9895d5cebe18697546d08580b3bd7faee8'}
    });
    done();
  });

  // TODO: add socket test
});
