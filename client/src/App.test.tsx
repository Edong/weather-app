import React from 'react';
import {cleanup, render, screen} from '@testing-library/react';
import App from './App';
import '@testing-library/jest-dom';
import renderer from 'react-test-renderer';

beforeAll(() => {
  render(<App />);
});

afterAll(() => {
  cleanup();
});

test('renders home section', () => {
  const section = screen.getByTestId('home-section');
  expect(section).toBeInTheDocument();
});

test('matches snapshot', () => {
  const tree = renderer.create(<App />).toJSON();
  expect(tree).toMatchSnapshot();
});
