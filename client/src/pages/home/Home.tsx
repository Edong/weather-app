import {Component} from 'react';
import './Home.css';
import {Input, PlusIcon, Widget} from '../../components';
import {config} from '../../common/config';
import {WeatherInterface, WidgetInterface} from '../../common/interfaces';
import {ApiEndpointEnum, StoreActions} from '../../common/enums';
import io from 'socket.io-client';
import {store, widgetsSelector} from '../../common/store';

const socket = io(config.wsUrl, {transports: ['websocket']});

export default class Home extends Component {
  state = {widgets: []};

  public componentDidMount(): void {
    this.setListener();
    this.websocketConnect();
    this.fetchWidgets();
  }

  private setListener(): void {
    store.subscribe(() => {
      this.setState({widgets: widgetsSelector()});
    });
  }

  private websocketConnect(): void {
    socket.connect();
    socket.on('connect', () => {
      console.log('WS Connection established.');
    });
    socket.on('weather-change', (data: WeatherInterface) => {
      store.dispatch({type: StoreActions.UPDATE_WEATHER, payload: data});
    });
    socket.on('disconnect', (data: any) => {
      console.log('WS Disconnected', data);
    });
  }

  // TODO: move to separate service
  private fetchWidgets(): void {
    fetch(`${config.apiUrl}${ApiEndpointEnum.GET_WIDGETS}`, {
      method: 'GET'
    })
      .then(response => response.json())
      .then(data => {
        store.dispatch({type: StoreActions.ADD_WIDGETS, payload: data});
      })
      // TODO: handle error
      .catch(error => console.log(error));
  }

  public render() {
    return (
      <section className="home-section" data-testid="home-section">
        <div className="home-upper-section">
          <Input />
          <PlusIcon />
        </div>
        <div className="home-lower-section">
          {this.state.widgets.map((widget: WidgetInterface) => (
            <Widget key={widget.id} widget={widget} />
          ))}
        </div>
      </section>
    );
  }
}
