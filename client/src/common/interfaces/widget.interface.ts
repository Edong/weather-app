export interface WidgetInterface {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  cityId: string;
  cityName: string;
}
