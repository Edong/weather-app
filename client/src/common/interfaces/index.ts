export * from './widget.interface';
export * from './weather.interface';
export * from './city.interface';
export * from './store.interface';
