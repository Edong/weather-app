import {CityInterface} from './city.interface';
import {WidgetInterface} from './widget.interface';
import {WeatherInterface} from './weather.interface';

export interface StoreInterface {
  city: CityInterface | null;
  widgets: WidgetInterface[];
  weathers: WeatherInterface[];
}
