export enum ApiEndpointEnum {
  GET_WIDGETS = '/widget',
  GET_WEATHER = '/weather',
  DELETE_WIDGET = '/widget',
  GET_CITIES = '/city',
  CREATE_WIDGET = '/widget'
}
