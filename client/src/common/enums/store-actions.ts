export enum StoreActions {
  ADD_CITY = 'city/addCity',
  ADD_WIDGETS = 'widget/addWidgets',
  UPDATE_WEATHER = 'weather/updateWeather',
  DELETE_WIDGET = 'widget/deleteWidget'
}
