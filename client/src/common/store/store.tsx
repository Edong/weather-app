import {reducer} from './reducer';
import {createStore} from 'redux';
import {StoreInterface} from '../interfaces';

const preloadedState: StoreInterface = {city: null, widgets: [], weathers: []};

export const store = createStore(reducer, preloadedState);
