import {StoreInterface, WeatherInterface, WidgetInterface} from '../interfaces';
import {StoreActions} from '../enums';

export function reducer(state: any, action: any): StoreInterface {
  switch (action.type) {
    case StoreActions.ADD_CITY:
      return {...state, city: action.payload};
    case StoreActions.ADD_WIDGETS:
      return {...state, widgets: [...state.widgets, ...action.payload]};
    case StoreActions.UPDATE_WEATHER:
      const index = state.weathers.findIndex((weather: WeatherInterface) => weather.cityId === action.payload.cityId);
      const weathers = [...state.weathers];
      if (index !== -1) {
        weathers[index] = action.payload;
      } else {
        weathers.push(action.payload);
      }
      return {...state, weathers};
    case StoreActions.DELETE_WIDGET:
      return {
        ...state,
        widgets: [...state.widgets.filter((widget: WidgetInterface) => widget.id !== action.payload.id)]
      };
    default:
      return state;
  }
}
