import {store} from './store';
import {CityInterface, WeatherInterface, WidgetInterface} from '../interfaces';

export const citySelector = (): CityInterface | null => {
  return store.getState().city;
};

export const widgetsSelector = (): WidgetInterface[] => {
  return store.getState().widgets;
};

export const weatherSelector = (cityId: string): WeatherInterface | undefined => {
  return store.getState().weathers.find((weather: WeatherInterface) => weather.cityId === cityId);
};
