import * as env from 'env-var';

interface Config {
  apiUrl: string;
  wsUrl: string;
}

export const config: Config = {
  apiUrl: env.get('REACT_APP_API_URL').required().asString(),
  wsUrl: env.get('REACT_APP_WS_URL').required().asString()
};
