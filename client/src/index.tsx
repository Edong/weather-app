import React from 'react';
import ReactDom from 'react-dom';
import './App.css';
import App from './App';
import {config} from 'dotenv';
import {Provider} from 'react-redux';
import {store} from './common/store';

config();

ReactDom.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
