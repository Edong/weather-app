import './RefreshIcon.css';
import {SyncSharp} from '@material-ui/icons';

export const RefreshIcon = () => {
  return (
    <div className="refresh-icon-container">
      <SyncSharp style={{fontSize: 30}} />
    </div>
  );
};
