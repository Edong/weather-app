import './SunIcon.css';
import {WbSunny} from '@material-ui/icons';

export const SunIcon = () => {
  return (
    <div className="sun-icon-container">
      <WbSunny style={{fontSize: 30}} />
    </div>
  );
};
