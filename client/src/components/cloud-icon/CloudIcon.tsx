import './CloudIcon.css';
import {FilterDramaSharp} from '@material-ui/icons';

export const CloudIcon = () => {
  return (
    <div className="cloud-icon-container">
      <FilterDramaSharp style={{fontSize: 30}} />
    </div>
  );
};
