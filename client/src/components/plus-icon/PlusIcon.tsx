import AddCircleOutlined from '@material-ui/icons/AddCircleOutlined';
import './PlusIcon.css';
import {config} from '../../common/config';
import {ApiEndpointEnum, StoreActions} from '../../common/enums';
import {citySelector, store} from '../../common/store';

export const PlusIcon = () => {
  const createWidget = () => {
    const city = citySelector();
    if (!city) {
      return;
    }
    const {id, name} = city;
    // TODO: move to separate service
    fetch(`${config.apiUrl}${ApiEndpointEnum.CREATE_WIDGET}`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({cityId: id, cityName: name})
    })
      .then(response => response.json())
      .then(data => {
        if (data && data.id) {
          store.dispatch({type: StoreActions.ADD_WIDGETS, payload: [data]});
        }
      })
      // TODO: handle error
      .catch(error => console.log(error));
  };

  return (
    <div className="plus-icon-container">
      <button onClick={() => createWidget()} className="icon-button">
        {/*TODO: implement shadow*/}
        <AddCircleOutlined style={{fontSize: 80, color: '#fd6a25'}} />
      </button>
    </div>
  );
};
