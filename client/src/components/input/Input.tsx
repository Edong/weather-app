import './Input.css';
import {useEffect, useState} from 'react';
import {config} from '../../common/config';
import {ApiEndpointEnum} from '../../common/enums';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {CityInterface} from '../../common/interfaces';
import {store} from '../../common/store';

export const Input = () => {
  const [inputValue, setInputValue] = useState('');
  const [value, setValue] = useState<CityInterface | null>(null);
  const [cities, setCities] = useState<CityInterface[]>([]);

  const onValueChange = (city: CityInterface | null) => {
    if (!city) {
      return;
    }
    setValue(city);
    store.dispatch({type: 'city/addCity', payload: city});
  };

  const fetchCities = () => {
    fetch(`${config.apiUrl}${ApiEndpointEnum.GET_CITIES}?searchTerm=${inputValue}`, {
      method: 'GET'
    })
      .then(response => response.json())
      .then(data => {
        setCities(data);
      })
      // TODO: handle error
      .catch(error => console.log(error));
  };

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      fetchCities();
    }, 1000);
    return () => clearTimeout(delayDebounceFn);
  }, [inputValue]);

  // TODO: fix placeholder
  return (
    <div>
      <Autocomplete
        value={value}
        onChange={(_, newValue) => onValueChange(newValue)}
        inputValue={inputValue}
        options={cities}
        getOptionLabel={(option: CityInterface) => option.name}
        className="input-form"
        renderInput={params => <TextField {...params} label="City" />}
        onInputChange={(_, newInputValue) => setInputValue(newInputValue)}
      />
    </div>
  );
};
