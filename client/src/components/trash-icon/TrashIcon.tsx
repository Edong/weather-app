import './TrashIcon.css';
import {DeleteForeverSharp} from '@material-ui/icons';

export const TrashIcon = () => {
  return (
    <div className="trash-icon-container">
      <DeleteForeverSharp style={{fontSize: 30}} />
    </div>
  );
};
