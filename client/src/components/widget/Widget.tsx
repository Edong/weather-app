import {Box} from '@material-ui/core';
import './Widget.css';
import {SunIcon} from '../sun-icon/SunIcon';
import {RefreshIcon} from '../refresh-icon/RefreshIcon';
import {TrashIcon} from '../trash-icon/TrashIcon';
import {CloudIcon} from '../cloud-icon/CloudIcon';
import {WeatherInterface, WidgetInterface} from '../../common/interfaces';
import {useEffect, useState} from 'react';
import {config} from '../../common/config';
import {ApiEndpointEnum, StoreActions} from '../../common/enums';
import {store, weatherSelector} from '../../common/store';

export const Widget = ({widget}: {widget: WidgetInterface}) => {
  const [weather, setWeather] = useState<WeatherInterface | Record<string, unknown>>({});

  const setListener = () => {
    store.subscribe(() => {
      const weather = weatherSelector(widget.cityId);
      if (weather) {
        setWeather(weather);
      }
    });
  };

  // TODO: move to separate service
  const fetchWeather = () => {
    fetch(`${config.apiUrl}${ApiEndpointEnum.GET_WEATHER}/${widget.cityId}`, {
      method: 'GET'
    })
      .then(response => response.json())
      .then(data => {
        store.dispatch({type: StoreActions.UPDATE_WEATHER, payload: {...data, cityId: widget.cityId}});
      })
      // TODO: handle error
      .catch(error => console.log(error));
  };

  // TODO: move to separate service
  const deleteWidget = () => {
    fetch(`${config.apiUrl}${ApiEndpointEnum.DELETE_WIDGET}/${widget.id}`, {
      method: 'DELETE'
    })
      .then(() => {
        store.dispatch({type: StoreActions.DELETE_WIDGET, payload: widget});
      })
      // TODO: handle error
      .catch(error => console.log(error));
  };

  useEffect(() => {
    fetchWeather();
    setListener();
  }, []);

  // TODO: remove white background from icons
  return (
    <div className="widget-container">
      <Box boxShadow={3} className="widget-box">
        <div className="widget-upper-section">
          <div className="widget-top">
            <div className="widget-top-left">
              {(weather?.cloudPercentage as number) < 50 ? <SunIcon /> : <CloudIcon />}
            </div>
            <div className="widget-top-right">
              <button onClick={() => fetchWeather()} className="icon-button">
                <RefreshIcon />
              </button>
              <button onClick={() => deleteWidget()} className="icon-button">
                <TrashIcon />
              </button>
            </div>
          </div>
          <div className="widget-middle">
            <span className="widget-middle-label">Temp {weather?.temperature || ''}</span>
          </div>
        </div>
        <div className="widget-lower-section">
          <span className="city-label">{widget.cityName}</span>
        </div>
      </Box>
    </div>
  );
};
