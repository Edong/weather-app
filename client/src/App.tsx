import React, {Component} from 'react';
import './App.css';
import Home from './pages/home/Home';

export default class App extends Component {
  render() {
    return <Home />;
  }
}
