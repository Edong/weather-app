Create .env file in server and client directory and paste environment variables from gitlab snippets.

Start app with docker-compose up command.

Frontend is exposed at port 3000.
